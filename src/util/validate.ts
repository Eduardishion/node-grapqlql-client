import {
	isEmail,
	isNumeric,
	isMobilePhone,
	isLength,
	isPort,
	isURL,
	isFloat,
	isInt
} from 'validator';

export const port = (value: string) => {
	return value && !isPort(value.trim()) ? 'Invalid port' : null;
};

export const url = (value: string) => {
	return value &&
		!isURL(value.trim(), {
			require_tld: false
		})
		? 'Invalid URL'
		: null;
};

export const floatValidation = (value: string) => {
	return value && !isFloat(value.trim()) ? 'Invalid float' : null;
};

export const intValidation = (value: string) => {
	return value && !isInt(value.trim()) ? 'Invalid integer' : null;
};

export const email = (value: string) => {
	return value && !isEmail(value.trim()) ? 'Invalid email' : null;
};

const isDirty = (value: string | undefined | 0) => {
	return value || value === 0;
};

export interface IValidation extends Object {
	[key: string]: any;
}

export const required = (requiredFields: string[], values: IValidation) => {
	return requiredFields.reduce<IValidation>((fields, field) => {
		return {
			...fields,
			...(isDirty(values[field]) ? undefined : { [field]: 'Required' })
		};
	}, {});
};

export const number = (value: string) => {
	return value && !isNumeric(value.trim()) ? 'Must be numeric' : null;
};

export const mobile = (value: string) => {
	return value && !isMobilePhone(value.trim(), 'en-ZA')
		? 'Invalid phone number'
		: null;
};

export const length = (value: string) => {
	return value && !isLength(value.trim(), 6, 20)
		? 'Password must be 6-20 digits'
		: null;
};

export const matchPassword = (password: string, confirmPassword: string) => {
	return password !== confirmPassword ? 'Passwords do not match' : null;
};
