import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import typeDefs from './typeDefs';

const cache = new InMemoryCache();

const client = new ApolloClient({
	cache,
	typeDefs,
	initializers: {
		isLoggedIn: () => false
	},
	link: new HttpLink({ uri: 'http://localhost:4000' })
});

export default client;
