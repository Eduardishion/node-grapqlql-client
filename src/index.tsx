import { MuiThemeProvider } from '@material-ui/core';
import { ApolloProvider } from 'react-apollo';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import client from './apollo/apolloClient';
import theme from './theme/theme';

ReactDOM.render(
	<ApolloProvider client={client}>
		<MuiThemeProvider theme={theme}>
			<App />
		</MuiThemeProvider>
	</ApolloProvider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
