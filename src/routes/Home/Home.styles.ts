import { Theme, createStyles } from '@material-ui/core';

const styles = (theme: Theme) =>
	createStyles({
		card: {
			minWidth: 275,
			maxWidth: 500,
			marginTop: 20,
			margin: 'auto',
			boxShadow: '0px 10px 30px -5px rgba(0, 0, 0, 0.3)',
			'&hover': {
				boxShadow: '0px 30px 100px -10px rgba(0, 0, 0, 0.4)'
			}
		},
		bullet: {
			display: 'inline-block',
			margin: '0 2px',
			transform: 'scale(0.8)'
		},
		title: {
			fontSize: 14
		},
		pos: {
			marginBottom: 12
		}
	});

export default styles;
