import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from '@reach/router';
import Loading from '../Loading/Loading';

import useLoadPCD from '../../components/PCDLoader/useLoadPCD';
import { Color } from 'three';

interface IProps extends RouteComponentProps {}

const pcd = (props: IProps) => {
	const [canvas, loading] = useLoadPCD(
		`${process.env.PUBLIC_URL}/pcds/PCD1.pcd`,
		{
			particalColor: '#ffffff'
		}
	);

	let loader;

	// if (mount === null) {
	// 	loader = <Loading />;
	// } else if (mount !== null) {
	// 	if (mount.children.length === 0) {
	// 		loader = <Loading />;
	// 	}
	// } else {
	// 	loader = <></>;
	// }

	console.log(loading);

	return (
		<>
			{loader}
			<div style={{ width: 800, height: 800 }} ref={canvas} />
		</>
	);
};

export default pcd;
