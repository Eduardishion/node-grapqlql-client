import * as THREE from 'three';

class PCDLoader extends THREE.Loader {
	manager?: THREE.LoadingManager;
	littleEndian: boolean;
	path: string;
	constructor(manager?: THREE.LoadingManager) {
		super();
		this.manager =
			manager !== undefined ? manager : THREE.DefaultLoadingManager;
		this.littleEndian = true;
		this.path = '';
	}

	public load(
		url: string,
		onLoad: (data: any) => void,
		onProgress: (request: ProgressEvent) => void,
		onError: (event: ErrorEvent) => void
	) {
		const scope = this;

		const loader = new THREE.FileLoader(scope.manager);
		loader.setPath(scope.path);
		loader.setResponseType('arraybuffer');
		loader.load(
			url,
			function(data) {
				try {
					onLoad(scope.parse(data, url));
				} catch (e) {
					if (onError) {
						onError(e);
					} else {
						throw e;
					}
				}
			},
			onProgress,
			onError
		);
	}

	public setPath(value: string) {
		this.path = value;
		return this;
	}

	public parse(data: any, url: string) {
		const parseHeader = (data: string) => {
			const PCDheader: any = {};
			const result1 = data.search(/[\r\n]DATA\s(\S*)\s/i);
			const result2: any = /[\r\n]DATA\s(\S*)\s/i.exec(
				data.substr(result1 - 1)
			);

			PCDheader.data = result2[1];
			PCDheader.headerLen = result2[0].length + result1;
			PCDheader.str = data.substr(0, PCDheader.headerLen);

			// remove comments

			PCDheader.str = PCDheader.str.replace(/\#.*/gi, '');

			// parse

			PCDheader.version = /VERSION (.*)/i.exec(PCDheader.str);
			PCDheader.fields = /FIELDS (.*)/i.exec(PCDheader.str);
			PCDheader.size = /SIZE (.*)/i.exec(PCDheader.str);
			PCDheader.type = /TYPE (.*)/i.exec(PCDheader.str);
			PCDheader.count = /COUNT (.*)/i.exec(PCDheader.str);
			PCDheader.width = /WIDTH (.*)/i.exec(PCDheader.str);
			PCDheader.height = /HEIGHT (.*)/i.exec(PCDheader.str);
			PCDheader.viewpoint = /VIEWPOINT (.*)/i.exec(PCDheader.str);
			PCDheader.points = /POINTS (.*)/i.exec(PCDheader.str);

			// evaluate

			if (PCDheader.version !== null)
				PCDheader.version = parseFloat(PCDheader.version[1]);

			if (PCDheader.fields !== null)
				PCDheader.fields = PCDheader.fields[1].split(' ');

			if (PCDheader.type !== null)
				PCDheader.type = PCDheader.type[1].split(' ');

			if (PCDheader.width !== null)
				PCDheader.width = parseInt(PCDheader.width[1]);

			if (PCDheader.height !== null)
				PCDheader.height = parseInt(PCDheader.height[1]);

			if (PCDheader.viewpoint !== null)
				PCDheader.viewpoint = PCDheader.viewpoint[1];

			if (PCDheader.points !== null)
				PCDheader.points = parseInt(PCDheader.points[1], 10);

			if (PCDheader.points === null)
				PCDheader.points = PCDheader.width * PCDheader.height;

			if (PCDheader.size !== null) {
				PCDheader.size = PCDheader.size[1]
					.split(' ')
					.map((x: string) => {
						return parseInt(x, 10);
					});
			}

			if (PCDheader.count !== null) {
				PCDheader.count = PCDheader.count[1]
					.split(' ')
					.map((x: string) => {
						return parseInt(x, 10);
					});
			} else {
				PCDheader.count = [];

				for (let i = 0, l = PCDheader.fields.length; i < l; i++) {
					PCDheader.count.push(1);
				}
			}

			PCDheader.offset = {};

			let sizeSum = 0;

			for (let i = 0, l = PCDheader.fields.length; i < l; i++) {
				if (PCDheader.data === 'ascii') {
					PCDheader.offset[PCDheader.fields[i]] = i;
				} else {
					PCDheader.offset[PCDheader.fields[i]] = sizeSum;
					sizeSum += PCDheader.size[i];
				}
			}

			// for binary only

			PCDheader.rowSize = sizeSum;

			return PCDheader;
		};

		const textData = THREE.LoaderUtils.decodeText(data);
		const PCDheader = parseHeader(textData);

		const position = [];
		const normal = [];
		const color = [];

		if (PCDheader.data === 'ascii') {
			const offset = PCDheader.offset;
			const pcdData = textData.substr(PCDheader.headerLen);
			const lines = pcdData.split('\n');

			for (let i = 0, l = lines.length; i < l; i++) {
				if (lines[i] === '') continue;

				const line = lines[i].split(' ');
				if (offset.x !== undefined) {
					position.push(parseFloat(line[offset.x]));
					position.push(parseFloat(line[offset.y]));
					position.push(parseFloat(line[offset.z]));
				}

				if (offset.rgb !== undefined) {
					const rgb = parseFloat(line[offset.rgb]);
					const r = (rgb >> 16) & 0x0000ff;
					const g = (rgb >> 8) & 0x0000ff;
					const b = (rgb >> 0) & 0x0000ff;
					color.push(r / 255, g / 255, b / 255);
				}

				if (offset.normal_x !== undefined) {
					normal.push(parseFloat(line[offset.normal_x]));
					normal.push(parseFloat(line[offset.normal_y]));
					normal.push(parseFloat(line[offset.normal_z]));
				}
			}
		}

		if (PCDheader.data === 'binary_compressed') {
			console.error(
				'THREE.PCDLoader: binary_compressed files are not supported'
			);
			return;
		}

		if (PCDheader.data === 'binary') {
			const dataview = new DataView(data, PCDheader.headerLen);
			const offset = PCDheader.offset;

			for (
				var i = 0, row = 0;
				i < PCDheader.points;
				i++, row += PCDheader.rowSize
			) {
				if (offset.x !== undefined) {
					position.push(
						dataview.getFloat32(row + offset.x, this.littleEndian)
					);
					position.push(
						dataview.getFloat32(row + offset.y, this.littleEndian)
					);
					position.push(
						dataview.getFloat32(row + offset.z, this.littleEndian)
					);
				}

				if (offset.rgb !== undefined) {
					color.push(dataview.getUint8(row + offset.rgb + 2) / 255.0);
					color.push(dataview.getUint8(row + offset.rgb + 1) / 255.0);
					color.push(dataview.getUint8(row + offset.rgb + 0) / 255.0);
				}

				if (offset.normal_x !== undefined) {
					normal.push(
						dataview.getFloat32(
							row + offset.normal_x,
							this.littleEndian
						)
					);
					normal.push(
						dataview.getFloat32(
							row + offset.normal_y,
							this.littleEndian
						)
					);
					normal.push(
						dataview.getFloat32(
							row + offset.normal_z,
							this.littleEndian
						)
					);
				}
			}
		}

		const geometry = new THREE.BufferGeometry();

		if (position.length > 0)
			geometry.addAttribute(
				'position',
				new THREE.Float32BufferAttribute(position, 3)
			);
		if (normal.length > 0)
			geometry.addAttribute(
				'normal',
				new THREE.Float32BufferAttribute(normal, 3)
			);
		if (color.length > 0)
			geometry.addAttribute(
				'color',
				new THREE.Float32BufferAttribute(color, 3)
			);

		geometry.computeBoundingSphere();

		// build material

		const material = new THREE.PointsMaterial({ size: 0.005 });

		if (color.length > 0) {
			material.vertexColors = THREE.VertexColors;
		} else {
			material.color.setHex(Math.random() * 0xffffff);
		}

		// build mesh

		const mesh = new THREE.Points(geometry, material);
		let name: string | RegExpExecArray = url
			.split('')
			.reverse()
			.join('');

		name = /([^\/]*)/.exec(name) as RegExpExecArray;

		name = name[1]
			.split('')
			.reverse()
			.join('');

		mesh.name = name;

		return mesh;
	}
}

export default PCDLoader;
