import React from 'react';
import { Transition, animated } from 'react-spring';
import { Typography } from '@material-ui/core';
import { ThemeStyle } from '@material-ui/core/styles/createTypography';

interface IProps {
	text: string;
	variant: ThemeStyle;
	fontSize?: number;
}

const reveal = (props: IProps) => (
	<div style={{ minHeight: 100, marginTop: 60 }}>
		<Transition
			native
			items={true}
			from={{ position: 'absolute', overflow: 'hidden', height: 0 }}
			enter={[{ height: 'auto' }]}
			leave={{ height: 0 }}
		>
			{show =>
				show &&
				(showProps => (
					<animated.div style={showProps}>
						<Typography
							noWrap={false}
							variant={props.variant}
							style={{ width: '100%', fontSize: props.fontSize }}
						>
							{props.text}
						</Typography>
					</animated.div>
				))
			}
		</Transition>
	</div>
);

export default reveal;
