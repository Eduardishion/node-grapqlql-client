import React from 'react';
import { Form, Field } from 'react-final-form';
import { FormApi } from 'final-form';
import { Typography, Button, Paper, Grid } from '@material-ui/core';

import TextField from '../UI/TextField';
import validate from './validate';

interface IProps {
	onSubmit: (
		values: object,
		form: FormApi,
		callback?: ((errors?: object | undefined) => void) | undefined
	) => void | object | Promise<object | undefined> | undefined;
}

const addTodo = (props: IProps) => {
	return (
		<Paper
			style={{
				margin: 'auto',
				marginTop: 20,
				maxWidth: 700,
				padding: 20,
				textAlign: 'center'
			}}
		>
			<Form
				onSubmit={props.onSubmit}
				validate={validate}
				render={({ handleSubmit, invalid }) => (
					<form onSubmit={handleSubmit}>
						<Grid container spacing={24}>
							<Grid item xs={12}>
								<Typography variant="h4">Add Todo</Typography>
							</Grid>
							<Grid item xs={6}>
								<Field
									name="title"
									component={TextField}
									type="text"
									label="Title"
								/>
							</Grid>
							<Grid item xs={6}>
								<Field
									name="description"
									component={TextField}
									type="text"
									label="Description"
								/>
							</Grid>
							<Grid item xs={12}>
								<Button
									type="submit"
									color="primary"
									variant="contained"
								>
									Add todo
								</Button>
							</Grid>
						</Grid>
					</form>
				)}
			/>
		</Paper>
	);
};

export default addTodo;
