import React, { Component, Suspense } from 'react';
import { Router, Link } from '@reach/router';
// import './App.css';

import Home from './routes/Home/Home';
import Loading from './routes/Loading/Loading';
import PointCloud from './routes/PointCloud/PointCloud';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Suspense fallback={Loading}>
					<Router>
						<Home path="/" />
						<Loading path="/loading" />
						<PointCloud path="/pcd" />
					</Router>
				</Suspense>
			</div>
		);
	}
}

export default App;
