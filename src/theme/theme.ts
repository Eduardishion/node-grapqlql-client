import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
	typography: {
		useNextVariants: true,
		fontSize: 12,
		fontFamily: 'Raleway, sans-serif'
	},
	palette: {
		primary: {
			main: '#3D55D1'
		},
		secondary: {
			main: '#D13D84'
		},
		text: {
			primary: '#FFF',
			secondary: '#24CF51'
		}
	}
});

export default theme;
